Описание структуры репозитория: https://www.evernote.com/l/AuvOc1JiuNdFar5e983bRvuk1iyDW-dI0Tc

Описание алгоритмов работы над тестовыми заданиями: https://www.evernote.com/l/AuuSF6NRnWFPuK-WeUQ8Yr4DIgHbr15dJVo

Описание работы с Git: https://www.evernote.com/l/AutoAbSECMxEYbWkS4_eCP9ozHB-AaVGzC0

Функции тестового фрейворка mocha: https://www.chaijs.com/api/bdd/