var chai = require('chai');
var expect = chai.expect;

var testInstance = require('./source.js');

describe('Шестнадцатиричные числа', function() {
    it('функция get48InHexFormat() должна возвращать 48', function() {
        expect(testInstance.get48InHexFormat()).to.equal(48);
    });
});