var chai = require('chai');
var expect = chai.expect;

var testInstance = require('./source.js');

describe('Работа с числами', function() {
    it('функция calculateHypotenuse(a, b) должна возвращать 5 при a=3 и b=4', function() {
        expect(testInstance.calculateHypotenuse(3, 4)).to.equal(5);
    });
});