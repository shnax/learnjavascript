var chai = require('chai');
var expect = chai.expect;

var testInstance = require('./source.js');

describe('Работа с числами', function() {
    it('функция isNonNumber(testInstance) должна возвращать true, когда testInstance равно NaN', function() {
        expect(testInstance.isNonNumber(NaN)).to.equal(true);
    });
    it('функция isNonNumber(testInstance) должна возвращать false, когда testInstance равно 7', function() {
        expect(testInstance.isNonNumber(7)).to.equal(false);
    });
});