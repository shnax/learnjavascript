var chai = require('chai');
var expect = chai.expect;

var testInstance = require('./source.js');

describe('Работа со строками', function() {
    it('функция getStringWithQuote() должна возвращать строку: Вы предпочитаете книги издательства "O\'Reilly", не правда ли?', function() {
        expect(testInstance.getStringWithQuote()).to.equal('Вы предпочитаете книги издательства "O\'Reilly", не правда ли?');
    });
});