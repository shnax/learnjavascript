var chai = require('chai');
var expect = chai.expect;

var testInstance = require('./source.js');

describe('Работа со строками', function() {
    it('функция getStringWithoutFirstWord(str) должна возвращать строку: "так хорошо сегодня" при str="не так хорошо сегодня"', function() {
        expect(testInstance.getStringWithoutFirstWord("не так хорошо сегодня")).to.equal("так хорошо сегодня");
    });
    it('функция getStringWithoutFirstWord(str) должна возвращать строку: "плащ" при str="Черный плащ"', function() {
        expect(testInstance.getStringWithoutFirstWord("Черный плащ")).to.equal("плащ");
    });
});