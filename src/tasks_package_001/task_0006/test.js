var chai = require('chai');
var expect = chai.expect;

var testInstance = require('./source.js');

describe('Работа со строками', function() {
    it('функция convertNumberToHexString(number) должна возвращать строку "f" при number=15', function() {
        expect(testInstance.convertNumberToHexString(15)).to.equal("f");
    });
    it('функция convertNumberToHexString(number) должна возвращать строку "3a" при number=58', function() {
        expect(testInstance.convertNumberToHexString(58)).to.equal("3a");
    });
});