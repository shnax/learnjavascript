var chai = require('chai');
var expect = chai.expect;

var testInstance = require('./source.js');

describe('Работа со строками', function() {
    it('функция formatMoney(money) должна возвращать строку "25.33" при money=25.33333333', function() {
        expect(testInstance.formatMoney(25.33333333)).to.equal("25.33");
    });
    it('функция formatMoney(money) должна возвращать строку "43423.49" при money=43423.49888832', function() {
        expect(testInstance.formatMoney(43423.46888832)).to.equal("43423.47");
    });
});