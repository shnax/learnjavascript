var chai = require('chai');
var expect = chai.expect;

var testInstance = require('./source.js');

describe('Работа с исключетильными ситуациями', function () {
  it('функция throwException() должна бросать ошибку', function () {
    expect(testInstance.throwException).to.throw(Error, "Все пропало!!!");
  });
});