var chai = require('chai');
var expect = chai.expect;

var testInstance = require('./source.js');

describe('Наследование через прототип', function () {
  it('функция createConstructors() должна создавать два конструктора, второй должен иметь прототип созданный с помощью первого', function () {
    var constructors = testInstance.createConstructors();

    var constructor1 = constructors[0]
    var constructor2 = constructors[1]

    var object1 = new constructor1()
    var object2 = new constructor2()

    expect(object1.f1()).to.equal("f1 из первого конструктора");
    expect(object1.f2()).to.equal("f2 из первого конструктора");

    expect(object2.f1()).to.equal("f1 из первого конструктора");
    expect(object2.f2()).to.equal("f2 из второго конструктора");
    expect(object2.f3()).to.equal("f3 из второго конструктора");

    expect(constructor2.prototype.f1()).to.equal("f1 из первого конструктора");
    expect(constructor2.prototype.f2()).to.equal("f2 из первого конструктора");
  });
});