var chai = require('chai');
var expect = chai.expect;

var testInstance = require('./source.js');

describe('Алгоритм сортировки пузырьком', function () {
  it('функция sortArray(array) должна возвращать отсортированыый массив, если параметер array содержит не пустой массив', function () {
    var inputArray = ["Шаурма", "Пылесос", "Вантус", "Армения", "Пахлава", "Изюм"];
    var expectedArray = ["Армения", "Вантус", "Изюм", "Пахлава", "Пылесос", "Шаурма"];
    expect(testInstance.sortArray(inputArray)).to.deep.equal(expectedArray);
  });

  it('функция sortArray(array) должна возвращать пустой массив, если параметер array содержит пустой массив', function () {
    expect(testInstance.sortArray([])).to.deep.equal([]);
  });

  it('функция sortArray(array) должна бросать ошибку, если параметер array содержит null', function () {
    expect(function () {
      testInstance.sortArray(null)
    }).to.throw(Error, "Input array can't be null or undefined");
  });

  it('функция sortArray(array) должна бросать ошибку, если параметер array содержит undefined', function () {
    expect(function () {
      testInstance.sortArray(undefined)
    }).to.throw(Error, "Input array can't be null or undefined");
  });

  it('функция sortArray(array) должна бросать ошибку, если параметер array не является массивом', function () {
    expect(function () {
      testInstance.sortArray(5)
    }).to.throw(TypeError, "Input parameter should have type: array, but has: number");
  });
});